resource "google_compute_network" "vpc_network" {
    name = "terraform-network"
}

resource "google_compute_instance" "default" {
    name         = "ubuntu-instance"
    machine_type = "n2-standard-2"
    zone         = "us-central1-a"

    tags = ["ghost-instance", "ubuntu-instance"]

    boot_disk {
        initialize_params {
            image = "debian-cloud/debian-11"
            labels = {
                ghost = "ghost"
            }
        }
    }

    network_interface {
        network = "default"
    }
    metadata_startup_script = "echo hi > /test.txt"

}