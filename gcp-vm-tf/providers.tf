terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.15.0"
    }
  }
}

provider "google" {
    project = "ghost-413518"
    region = "us-west4"
    credentials = "./keys.json"
}