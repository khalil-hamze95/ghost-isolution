# Terraform

# Create Service Account
1- Create a service account named `terraform` with the `compute admin` role for VM creation and deletion.
2- Generate a key for the `terraform` service account and download it.


## Terraform configs
The `gcp-vm-tf` contain all configuration files related to create the gcp instance.
`providers`: google provider, version and keys
`main`: configuration of instance that you need to create (**specs**)
`outputs`: The output that you need to see when the instance is created

![](./images/service-account.png)


# Ghost Installation
In this section you will see the steps to install ghost app on ubuntu machine. Then you will see these steps configured under an ansible playbook that contain all modules to run it automatically on the server.

## Install Ghost Application on ubuntu
### Nginx
```
sudo apt-get install nginx
sudo ufw allow 'Nginx Full'
```
### MySql
```
sudo apt-get install mysql-server

sudo mysql
# Update permissions
ALTER USER 'ghost_user'@'localhost' IDENTIFIED BY P@ssw0rd;
# Reread permissions
FLUSH PRIVILEGES;
# exit mysql
exit
```

### Install Ghost CLI
Recommended node v18 hydrogen
```
sudo npm install ghost-cli@latest -g
```

go to the folder target and execute:
`ghost install`

## Install Ghost Application using Ansible
I impelement an ansible playbook that contain all the previous steps. The Ansible structure is dynamic and includes inventories, variables, and main playbooks.

### Group Vars
The `ansible/group_vars` directory contains a file with all the required variables. Configure this file for the desired server:
- SERVER_IP
- SERVER_USERNAME
- SERVER_KEY_PATH
  
### Inventory
Under `ansible/inventories` directory includes the hosts destination file.

### Playbook
the `main.yml` file in the Ansible playbook contains the modules necessary for installing Ghost prerequisites.

Result of Ansible playbook on my personal instance:

![](./images/ansible-result.png)

# Pipeline CICD
The pipeline CICD will install and deploy the Ghost app using docker container.
## Runner
A runner is registred on my instance and linked to the repository. Ther runner registred is a docker runner with these configurations:
to register the runner run this command below:

`docker run --rm -it -v /srv/gitlab-runner/ghost-runner-config:/etc/gitlab-runner gitlab/gitlab-runner register`

```
URL: https://gitlab.com
Token: ********
description: ghost runner
tags: isolutions, ghost
executer: docker
image: docker:stable
```

After make some configuration in the `config.toml` file generated you will obtain:
```
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "ghost runner"
  url = "https://gitlab.com/"
  id = 32335681
  token = "zexQCzttzXJsb3i7bwhy"
  token_obtained_at = 2024-02-06T21:10:38Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true # Changed
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    network_mtu = 0
```
Finally to run the runner:

`docker run -d --name gitlab-runner --restart always   -v /srv/gitlab-runner/ghost-runner-config/:/etc/gitlab-runner   -v /var/run/docker.sock:/var/run/docker.sock   gitlab/gitlab-runner:latest`


![](./images/runner-result.png)

## CICD
In the `.gitlab-ci.yml` you will see one stage deploy because already the image is ready to be deployed.

After last push this is the pipeline result:

![](./images/pipeline-result.png)

If you can see now the below image show you the ghost application running on the destinated instance

![](./images/ghost-docker-result.png)